package com.scb10x.xboard.features.topic

import com.scb10x.xboard.core.extension.decode
import com.scb10x.xboard.features.topic.data.TopicEntity
import com.scb10x.xboard.features.topic.presentation.TopicView
import java.util.*

data class Topic(
    val id: Long,
    val title: String,
    val message: String,
    val ipAddress: String,
    val totalComment : Long,
    val categoryID: Long,
    val created: Date?
) {
    fun toEntity() = TopicEntity(id, title, message, ipAddress, totalComment, categoryID, created)

    fun toView() = TopicView(id, title.decode(), message.decode(), ipAddress, totalComment, categoryID, created)
}