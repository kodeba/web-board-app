package com.scb10x.xboard.features.topic.presentation

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.inflate
import kotlinx.android.synthetic.main.list_item_category.view.*
import kotlin.properties.Delegates

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    internal var collection: List<CategoryView> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListener: (CategoryView) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.list_item_category))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(collection[position], clickListener)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(categoryView: CategoryView, clickListener: (CategoryView) -> Unit) {
            with(itemView) {
                category_title.text = categoryView.title
                setOnClickListener { clickListener(categoryView) }
            }
        }
    }
}