package com.scb10x.xboard.core.di

import com.scb10x.xboard.core.navigation.Navigator
import org.koin.dsl.module.module

val navigationModule = module {
    single { Navigator() }
}