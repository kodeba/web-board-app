package com.scb10x.xboard.features.topic

import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.features.topic.data.CommentRepository
import io.reactivex.Flowable

class CreateComment(override val repository: CommentRepository) : UseCase<Comment, Flowable<Comment>>(repository) {
    override fun run(param: Comment): Flowable<Comment> = repository.createComment(param)
}