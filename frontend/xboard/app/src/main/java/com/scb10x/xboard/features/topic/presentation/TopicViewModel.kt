package com.scb10x.xboard.features.topic.presentation

import androidx.lifecycle.MutableLiveData
import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.core.platform.BaseViewModel
import com.scb10x.xboard.features.topic.CreateTopic
import com.scb10x.xboard.features.topic.GetAllTopic
import com.scb10x.xboard.features.topic.GetTopicByID
import com.scb10x.xboard.features.topic.Topic
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class TopicViewModel(
    private val getAllTopic: GetAllTopic,
    private val getTopicByID: GetTopicByID,
    private val createTopic: CreateTopic
) : BaseViewModel() {

    var topics: MutableLiveData<List<TopicView>> = MutableLiveData()
    var topic: MutableLiveData<TopicView> = MutableLiveData()
    var createdTopic: MutableLiveData<TopicView> = MutableLiveData()

    private fun handleTopics(topics: List<Topic>?) {
        this.topics.value = topics?.map { it.toView() }
    }

    private fun handleTopic(topic: Topic?) {
        this.topic.value = topic?.toView()
    }

    private fun handleCreatedTopic(topic: Topic?) {
        this.createdTopic.value = topic?.toView()
    }

    fun loadTopics(offset: Int = 0, categoryID: Long = 0L) {
        disposable.add(
            getAllTopic
                .run(GetAllTopic.Param(offset, categoryID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading() }
                .doOnTerminate { done() }
                .subscribe(::handleTopics, ::handleError)
        )
    }

    fun loadTopic(topicID: Long) {
        disposable.add(
            getTopicByID
                .run(topicID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading() }
                .doOnTerminate { done() }
                .subscribe(::handleTopic, ::handleError)
        )
    }

    fun saveTopic(topic: Topic) {
        disposable.add(
            createTopic
                .run(topic)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading() }
                .doOnTerminate { done() }
                .subscribe(::handleCreatedTopic, ::handleError)
        )
    }


}
