package com.scb10x.xboard.features.topic.data

import com.scb10x.xboard.core.data.BaseService
import io.reactivex.Flowable
import retrofit2.Retrofit

class TopicService(retrofit: Retrofit) : BaseService<TopicApi>(retrofit), TopicApi{
    override val api: TopicApi = createApi()

    override fun topics(offset: Int, categoryID: Long): Flowable<List<TopicEntity>> = api.topics(offset, categoryID)
    override fun topic(topicID: Long): Flowable<TopicEntity> = api.topic(topicID)
    override fun createTopic(topicEntity: TopicEntity): Flowable<TopicEntity> = api.createTopic(topicEntity)
}