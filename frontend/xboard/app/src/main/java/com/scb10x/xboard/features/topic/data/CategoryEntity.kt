package com.scb10x.xboard.features.topic.data

import com.scb10x.xboard.features.topic.Category
import java.util.*

data class CategoryEntity(
    private val id: Long,
    private val title: String,
    private val description: String,
    private val created: Date?
) {
    fun toCategory() = Category(id, title, description, created)
}