package com.scb10x.xboard.features.topic.presentation

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.failure
import com.scb10x.xboard.core.extension.observe
import com.scb10x.xboard.core.extension.useDefaultHorizontalLayout
import com.scb10x.xboard.core.extension.useDefaultVerticleLayout
import com.scb10x.xboard.core.navigation.Navigator
import com.scb10x.xboard.core.platform.BaseFragment
import kotlinx.android.synthetic.main.fragment_topic.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class TopicsFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.fragment_topic

    private val topicViewModel: TopicViewModel by viewModel()
    private val topicsAdapter: TopicsAdapter by inject()

    private val categoryViewModel: CategoryViewModel by viewModel()
    private val categoryAdapter: CategoryAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(topicViewModel) {
            observe(isLoading, ::handleProgress)
            observe(topics, ::renderTopics)
            failure(error, {
                it?.printStackTrace()
            })
        }

        with(categoryViewModel) {
            observe(isLoading, ::handleProgress)
            observe(categories, ::renderCategories)
            failure(error, {
                it?.printStackTrace()
            })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeView()
        categoryViewModel.loadCategories()
        topicViewModel.loadTopics()
    }

    private fun initializeView() {
        initializeList()
        initializeButton()
    }

    private fun initializeList() {
        category_list.useDefaultHorizontalLayout(activity?.baseContext)
        category_list.adapter = categoryAdapter
        categoryAdapter.clickListener = { navigator.showTopicCategory(activity!!, it) }

        topic_list.useDefaultVerticleLayout(activity?.baseContext)
        topic_list.adapter = topicsAdapter
        topicsAdapter.clickListener = { navigator.showTopicDetail(activity!!, it) }
    }

    private fun initializeButton() {
        create_button.setOnClickListener { navigator.showTopicCreation(activity!!, CategoryView(0L)) }
    }

    private fun renderTopics(topics: List<TopicView>?) {
        topicsAdapter.collection = topics.orEmpty()
    }

    private fun renderCategories(categories: List<CategoryView>?) {
        categoryAdapter.collection = categories.orEmpty()
    }


}
