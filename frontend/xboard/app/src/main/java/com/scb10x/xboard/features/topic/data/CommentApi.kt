package com.scb10x.xboard.features.topic.data

import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface CommentApi {
    companion object {
        private const val OFFSET = 10
        private const val PARAM_OFFSET = "offset"
        private const val PARAM_TOPIC_ID = "topic_id"
        private const val COMMENT_ROOT = "comment"
        private const val COMMENT_LIST = "$COMMENT_ROOT?"
    }

    @GET(COMMENT_LIST)
    fun comments(
        @Query(PARAM_TOPIC_ID) topicID: Long,
        @Query(PARAM_OFFSET) offset: Int = 0
    ): Flowable<List<CommentEntity>>

    @POST(COMMENT_ROOT)
    fun createComment(@Body commentEntity: CommentEntity): Flowable<CommentEntity>
}