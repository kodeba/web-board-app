package com.scb10x.xboard.core.di

import com.google.gson.GsonBuilder
import com.scb10x.xboard.features.topic.data.*
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


val dataModule = module {
    single {
        Retrofit.Builder()
            .baseUrl("http://ec2-3-19-70-69.us-east-2.compute.amazonaws.com:8080/api/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder().disableHtmlEscaping()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                        .create()
                )
            )
            .build()
    }

    single { TopicService(get()) }
    factory { TopicRepository.Network(get()) as TopicRepository }

    single { CommentService(get()) }
    factory { CommentRepository.Network(get()) as CommentRepository }

    single { CategoryService(get()) }
    factory { CategoryRepository.Network(get()) as CategoryRepository }
}