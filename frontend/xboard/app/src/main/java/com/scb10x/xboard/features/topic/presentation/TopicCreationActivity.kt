package com.scb10x.xboard.features.topic.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.scb10x.xboard.core.platform.BaseActivity
import com.scb10x.xboard.core.platform.BaseFragment
import kotlinx.android.synthetic.main.toobar.*

class TopicCreationActivity : BaseActivity() {

    companion object {

        private const val INTENT_PARAM_CATEGORY = "intent_param_category"

        fun callingIntent(context: Context, categoryView: CategoryView): Intent {
            val intent = Intent(context, TopicCreationActivity::class.java)
            intent.putExtra(INTENT_PARAM_CATEGORY, categoryView)
            return intent
        }
    }

    override fun fragment(): BaseFragment = TopicCreationFragment.forCategory(intent.getParcelableExtra(INTENT_PARAM_CATEGORY))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(supportActionBar!!){
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        toolbar.setNavigationOnClickListener{ finish() }
    }
}