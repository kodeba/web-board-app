package com.scb10x.xboard.features.topic.presentation

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.inflate
import com.scb10x.xboard.core.extension.timeAgo
import kotlinx.android.synthetic.main.list_item_topic.view.*
import kotlin.properties.Delegates

class TopicsAdapter : RecyclerView.Adapter<TopicsAdapter.ViewHolder>() {

    internal var collection: List<TopicView> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListener: (TopicView) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.list_item_topic))


    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(collection[position], clickListener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(topicView: TopicView, clickListener: (TopicView) -> Unit) {

            with(itemView) {
                topic_title.text = topicView.title
                topic_message.text = topicView.message
                topic_owner.text = topicView.ipAddress
                topic_total_comments.text = "${topicView.totalComment} comments"
                topic_last_update.text = topicView.created?.timeAgo()

                setOnClickListener { clickListener(topicView) }
            }

        }
    }
}