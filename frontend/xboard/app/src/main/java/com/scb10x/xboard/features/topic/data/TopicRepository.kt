package com.scb10x.xboard.features.topic.data

import com.scb10x.xboard.core.data.IRepository
import com.scb10x.xboard.features.topic.Topic
import io.reactivex.Flowable

interface TopicRepository : IRepository {
    fun topics(offset : Int = 0, categoryID : Long = 0L) : Flowable<List<Topic>>
    fun topic(topicID : Long) : Flowable<Topic>
    fun createTopic(topic: Topic) : Flowable<Topic>

    class Network(private val service: TopicService) : TopicRepository {
        override fun topics(offset: Int, categoryID: Long): Flowable<List<Topic>> = service.topics(offset, categoryID).map { it.map { t -> t.toTopic() } }

        override fun topic(topicID: Long): Flowable<Topic> = service.topic(topicID).map { it.toTopic() }

        override fun createTopic(topic: Topic): Flowable<Topic> = service.createTopic(topic.toEntity()).map { it.toTopic() }
    }
}