package com.scb10x.xboard.features.topic.data

import com.scb10x.xboard.core.data.BaseService
import io.reactivex.Flowable
import retrofit2.Retrofit

class CategoryService(retrofit: Retrofit) : BaseService<CategoryApi>(retrofit), CategoryApi {

    override val api: CategoryApi = createApi()

    override fun categories(): Flowable<List<CategoryEntity>> = api.categories()
}