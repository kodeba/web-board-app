package com.scb10x.xboard.features.topic

import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.features.topic.data.CategoryRepository
import io.reactivex.Flowable

class GetAllCategory(override val repository: CategoryRepository) : UseCase<UseCase.None, Flowable<List<Category>>>(repository) {
    override fun run(param: None): Flowable<List<Category>> = repository.categories()
}