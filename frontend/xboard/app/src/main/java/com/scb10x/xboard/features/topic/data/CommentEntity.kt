package com.scb10x.xboard.features.topic.data

import com.google.gson.annotations.SerializedName
import com.scb10x.xboard.features.topic.Comment
import java.util.*


data class CommentEntity(
    private val id: Long,
    private val message: String,
    @SerializedName("ip_address") private val ipAddress: String,
    @SerializedName("topic_id") private val topicID: Long,
    private val created: Date?
) {
    fun toComment() = Comment(id, message, ipAddress, topicID, created)
}