package com.scb10x.xboard.features.topic.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputType
import android.view.View
import com.jakewharton.rxbinding3.widget.textChanges
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.*
import com.scb10x.xboard.core.platform.BaseFragment
import com.scb10x.xboard.features.topic.Comment
import kotlinx.android.synthetic.main.fragment_topic_detail.*
import org.koin.android.ext.android.inject
import retrofit2.HttpException
import java.net.SocketException

@SuppressLint("CheckResult")
class TopicDetailFragment : BaseFragment() {

    companion object {
        private const val PARAM_TOPIC = "param_topic"

        fun forTopic(topicView: TopicView): TopicDetailFragment {
            val topicDetailFragment = TopicDetailFragment()
            val bundle = Bundle()
            bundle.putParcelable(PARAM_TOPIC, topicView)

            topicDetailFragment.arguments = bundle

            return topicDetailFragment
        }
    }

    override fun layoutId(): Int = R.layout.fragment_topic_detail

    private val commentsAdapter: CommentsAdapter by inject()
    private val commentViewModel: CommentViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(commentViewModel) {
            observe(comments, ::handleComments)
            observe(createdComment, ::handleCreatedComment)
            observe(isLoading, ::handleProgress)
            failure(error, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (firstTimeCreated(savedInstanceState)) {
            initializeView((arguments?.get(PARAM_TOPIC) as TopicView))
        } else {
            initializeView((arguments!![PARAM_TOPIC] as TopicView))
        }
    }

    private fun initializeView(topicView: TopicView) {

        initializeCommentBox(topicView)
        initializeTopic(topicView)
        initializeComments(topicView)
    }


    private fun initializeCommentBox(topicView: TopicView) {
        with(comment_edit_text) {
            maxLines = 1
            inputType = InputType.TYPE_CLASS_TEXT
        }

        comment_edit_text
            .textChanges()
            .map { it.isNotBlank() }
            .subscribe { post_button.isEnabled = it }

        post_button.setOnClickListener { postComment(topicView) }
    }

    private fun initializeTopic(topicView: TopicView) {
        topic.requestFocus()

        with(topicView) {
            topic_title.text = title
            topic_owner.text = ipAddress
            topic_message.text = message
            topic_last_update.text = created?.timeAgo()
        }
    }

    private fun initializeComments(topicView: TopicView) {
        comment_list.useDefaultVerticleLayout(activity?.baseContext)
        comment_list.adapter = commentsAdapter

        commentViewModel.loadComments(topicView.id)
    }

    private fun handleComments(comments: List<CommentView>?) {
        commentsAdapter.collection = comments.orEmpty()
    }

    private fun handleFailure(error: Throwable?) {
        when (error) {
            is SocketException, is HttpException -> notify(R.string.network_error)
            else -> notify(R.string.unknown_error)
        }
    }

    private fun postComment(topicView: TopicView) {
        comment_edit_text.requestFocus()
        comment_edit_text.hideKeyboard(activity)

        commentViewModel.saveComment(
            CommentView(
                message = comment_edit_text.text.toString(),
                topicID = topicView.id
            ).toComment()
        )
    }

    private fun handleCreatedComment(comment: CommentView?) {
        comment_edit_text.text.clear()
        scroll_container.scrollTo(0,0)
        val comments = mutableListOf(comment!!)

        comments.addAll(commentsAdapter.collection)

        commentsAdapter.collection = comments.toList()

    }


}