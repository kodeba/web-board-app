package com.scb10x.xboard.features.topic.data

import com.scb10x.xboard.core.data.IRepository
import com.scb10x.xboard.features.topic.Comment
import io.reactivex.Flowable

interface CommentRepository : IRepository {

    fun comments(topicID : Long, offset : Int = 0) : Flowable<List<Comment>>
    fun createComment(comment: Comment): Flowable<Comment>

    class Network(private val service: CommentService) : CommentRepository {
        override fun comments(topicID: Long, offset: Int) = service.comments(topicID, offset).map { l -> l.map { it.toComment() } }
        override fun createComment(comment: Comment): Flowable<Comment> = service.createComment(comment.toEntity()).map { it.toComment() }
    }

}