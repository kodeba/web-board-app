package com.scb10x.xboard.features.topic.presentation

import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.inflate
import kotlinx.android.synthetic.main.list_item_category.view.*
import kotlin.properties.Delegates

class SelectableCategoryAdapter : RecyclerView.Adapter<SelectableCategoryAdapter.ViewHolder>() {

    internal var collection: List<CategoryView> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var selectedPosition:Int by Delegates.observable(-1) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal lateinit var selectedItem : CategoryView

    internal var clickListener: (CategoryView) -> Unit = { _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.list_item_category))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(collection[position])

        with(holder.itemView) {
            when(selectedPosition) {
                position -> {
                    category_title.background = ContextCompat.getDrawable(context, R.drawable.purple_round_w_boarder)
                    category_title.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                }
                else -> {
                    category_title.background = ContextCompat.getDrawable(context, R.drawable.white_round_w_boarder)
                    category_title.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                }
            }

            setOnClickListener {
                selectedPosition = position
                selectedItem = collection[position]
                clickListener(selectedItem)
            }
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(categoryView: CategoryView) {
            with(itemView) {
                category_title.text = categoryView.title
            }
        }
    }
}