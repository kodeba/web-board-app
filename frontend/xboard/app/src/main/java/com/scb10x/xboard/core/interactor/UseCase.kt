package com.scb10x.xboard.core.interactor

import com.scb10x.xboard.core.data.IRepository

abstract class UseCase<in T, out R>(protected open val repository: IRepository) {

    abstract fun run(param: T) : R

    class None
}