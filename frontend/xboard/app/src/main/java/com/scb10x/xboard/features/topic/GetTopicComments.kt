package com.scb10x.xboard.features.topic

import com.scb10x.xboard.core.data.IRepository
import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.features.topic.data.CommentRepository
import io.reactivex.Flowable

class GetTopicComments(override val repository: CommentRepository) : UseCase<GetTopicComments.Param, Flowable<List<Comment>>>(repository) {
    override fun run(param: Param): Flowable<List<Comment>> = repository.comments(param.topicID, param.offset)

    class Param(val topicID : Long, val offset : Int = 0)
}