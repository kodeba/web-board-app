package com.scb10x.xboard.features.topic.presentation

import androidx.lifecycle.MutableLiveData
import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.core.platform.BaseViewModel
import com.scb10x.xboard.features.topic.Category
import com.scb10x.xboard.features.topic.GetAllCategory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CategoryViewModel(private val getAllCategory: GetAllCategory) : BaseViewModel() {
    var categories : MutableLiveData<List<CategoryView>> = MutableLiveData()

    fun handleCategories(categories : List<Category>?) {
        this.categories.value = categories?.map { it.toView() }
    }

    fun loadCategories() {
        disposable.add(
            getAllCategory
                .run(UseCase.None())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading() }
                .doOnTerminate { done() }
                .subscribe( ::handleCategories, ::handleError)
        )
    }
}