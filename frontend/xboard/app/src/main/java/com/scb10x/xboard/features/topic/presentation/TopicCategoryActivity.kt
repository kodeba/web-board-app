package com.scb10x.xboard.features.topic.presentation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.scb10x.xboard.core.platform.BaseActivity
import com.scb10x.xboard.core.platform.BaseFragment
import kotlinx.android.synthetic.main.toobar.*

class TopicCategoryActivity : BaseActivity() {

    companion object {
        private const val PARAM_INTENT_CATEGORY = "param_intent_category"

        fun callingIntent(context: Context, categoryView: CategoryView) : Intent {
            val intent = Intent(context, TopicCategoryActivity::class.java)
            intent.putExtra(PARAM_INTENT_CATEGORY, categoryView)
            return intent
        }
    }

    override fun fragment(): BaseFragment = TopicCategoryFragment.forCategory(intent.getParcelableExtra(PARAM_INTENT_CATEGORY))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(supportActionBar!!){
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        toolbar.setNavigationOnClickListener{ finish() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            1 -> when(resultCode){
                Activity.RESULT_OK -> reloadFragment()
            }
        }
    }
}