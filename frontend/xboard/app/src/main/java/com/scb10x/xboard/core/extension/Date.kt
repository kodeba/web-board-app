package com.scb10x.xboard.core.extension

import com.github.marlonlom.utilities.timeago.TimeAgo
import com.github.marlonlom.utilities.timeago.TimeAgoMessages
import java.util.*

fun Date.timeAgo() : String  {
    val localeBylanguageTag = Locale.forLanguageTag("th")
    val messages = TimeAgoMessages.Builder().withLocale(localeBylanguageTag).build()

    return TimeAgo.using(this.time, messages)
}