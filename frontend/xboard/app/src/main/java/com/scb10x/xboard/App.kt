package com.scb10x.xboard

import android.app.Application
import com.scb10x.xboard.core.di.dataModule
import com.scb10x.xboard.core.di.navigationModule
import com.scb10x.xboard.core.di.topicModule
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(dataModule, topicModule, navigationModule))
    }
}