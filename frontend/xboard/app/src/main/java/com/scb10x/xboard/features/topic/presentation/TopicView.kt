package com.scb10x.xboard.features.topic.presentation
import android.os.Parcelable
import com.scb10x.xboard.core.extension.encode
import com.scb10x.xboard.features.topic.Topic
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class TopicView(
    val id: Long = 0L,
    val title: String,
    val message: String,
    val ipAddress: String = "",
    val totalComment: Long = 0,
    val categoryID: Long = 1,
    val created: Date? = null
) : Parcelable {
    fun toTopic() = Topic(id, title.encode(), message.encode(), ipAddress, totalComment, categoryID, created)
}