package com.scb10x.xboard.features.topic.data

import com.google.gson.annotations.SerializedName
import com.scb10x.xboard.features.topic.Topic
import java.util.*

class TopicEntity(
    private val id: Long,
    private val title: String,
    private val message: String,
    @SerializedName("ip_address") private val ipAddress: String,
    @SerializedName("total_comment") private val totalComment: Long,
    @SerializedName("category_id") private val categoryID: Long,
    private val created: Date?
) {
    fun toTopic() = Topic(id, title, message, ipAddress, totalComment, categoryID, created)
}