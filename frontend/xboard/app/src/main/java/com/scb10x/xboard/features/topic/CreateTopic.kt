package com.scb10x.xboard.features.topic

import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.features.topic.data.TopicRepository
import io.reactivex.Flowable
import java.lang.IllegalArgumentException

class CreateTopic(override val repository: TopicRepository) : UseCase<Topic, Flowable<Topic>>(repository) {
    override fun run(param: Topic): Flowable<Topic>  {
        if(param.title.isBlank() || param.message.isBlank())
            return Flowable.error<Topic>(IllegalArgumentException())

        return repository.createTopic(param)
    }
}