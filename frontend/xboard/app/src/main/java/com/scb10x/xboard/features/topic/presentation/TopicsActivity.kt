package com.scb10x.xboard.features.topic.presentation

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.scb10x.xboard.core.platform.BaseActivity
import com.scb10x.xboard.core.platform.BaseFragment

class TopicsActivity : BaseActivity() {
    override fun fragment(): BaseFragment = TopicsFragment()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            1 -> when(resultCode){
                Activity.RESULT_OK -> reloadFragment()
            }
        }
    }

}
