package com.scb10x.xboard.features.topic

import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.features.topic.data.TopicRepository
import io.reactivex.Flowable

class GetTopicByID(override val repository: TopicRepository) : UseCase<Long, Flowable<Topic>>(repository) {
    override fun run(param: Long): Flowable<Topic> = repository.topic(param)
}