package com.scb10x.xboard.features.topic.data

import com.scb10x.xboard.core.data.IRepository
import com.scb10x.xboard.features.topic.Category
import io.reactivex.Flowable

interface CategoryRepository : IRepository {
    fun categories() : Flowable<List<Category>>

    class Network(private val service: CategoryService) : CategoryRepository {
        override fun categories(): Flowable<List<Category>> = service.categories().map { l -> l.map { it.toCategory() } }
    }
}