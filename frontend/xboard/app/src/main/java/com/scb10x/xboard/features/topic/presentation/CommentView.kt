package com.scb10x.xboard.features.topic.presentation

import android.os.Parcelable
import com.scb10x.xboard.core.extension.encode
import com.scb10x.xboard.features.topic.Comment
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class CommentView(
    val id: Long = 0L,
    val message: String,
    val ipAddress: String = "0.0.0.0",
    val topicID: Long,
    val created: Date? = null
) : Parcelable {
    fun toComment() = Comment(id, message.encode(), ipAddress, topicID, created)
}