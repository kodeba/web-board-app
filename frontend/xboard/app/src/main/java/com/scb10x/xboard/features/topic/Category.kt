package com.scb10x.xboard.features.topic

import com.scb10x.xboard.features.topic.data.CategoryEntity
import com.scb10x.xboard.features.topic.presentation.CategoryView
import java.util.*

data class Category(
    val id: Long,
    val title: String,
    val description: String,
    val created: Date?
) {
    fun toEntity() = CategoryEntity(id, title, description, created)
    fun toView() = CategoryView(id, title, description, created)
}