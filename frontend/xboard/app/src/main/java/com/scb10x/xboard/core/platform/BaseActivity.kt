package com.scb10x.xboard.core.platform

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.scb10x.xboard.R
import com.scb10x.xboard.R.id
import com.scb10x.xboard.core.extension.inTransaction
import kotlinx.android.synthetic.main.toobar.*

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout)
        setSupportActionBar(toolbar)
        addFragment(savedInstanceState)
    }

    override fun onBackPressed() {
        (supportFragmentManager.findFragmentById(
            id.container) as BaseFragment).onBackPressed()
        super.onBackPressed()
    }

    private fun addFragment(savedInstanceState: Bundle?) =
        savedInstanceState ?: supportFragmentManager.inTransaction { add(
            id.container, fragment()) }

    internal fun reloadFragment() {
        supportFragmentManager.inTransaction {
            replace(id.container, fragment())
        }
    }

    abstract fun fragment(): BaseFragment
}