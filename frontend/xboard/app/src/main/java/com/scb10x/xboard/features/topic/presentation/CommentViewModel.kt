package com.scb10x.xboard.features.topic.presentation

import androidx.lifecycle.MutableLiveData
import com.scb10x.xboard.core.platform.BaseViewModel
import com.scb10x.xboard.features.topic.Comment
import com.scb10x.xboard.features.topic.CreateComment
import com.scb10x.xboard.features.topic.GetTopicComments
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CommentViewModel(
    private val createComment: CreateComment,
    private val getTopicComments: GetTopicComments
) : BaseViewModel() {

    var createdComment: MutableLiveData<CommentView> = MutableLiveData()
    var comments: MutableLiveData<List<CommentView>> = MutableLiveData()

    private fun handleCreatedComment(comment: Comment?) {
        createdComment.value = comment?.toView()
    }

    private fun handleComments(comments : List<Comment>?){
        this.comments.value = comments?.map { it.toView() }
    }

    fun saveComment(comment: Comment) {
        disposable.add(
            createComment
                .run(comment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading() }
                .doOnTerminate { done() }
                .subscribe(::handleCreatedComment, ::handleError)
        )
    }

    fun loadComments(topicID : Long) {
        disposable.add(
            getTopicComments
                .run(GetTopicComments.Param(topicID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { loading() }
                .doOnTerminate { done() }
                .subscribe(::handleComments, ::handleError)
        )
    }
}