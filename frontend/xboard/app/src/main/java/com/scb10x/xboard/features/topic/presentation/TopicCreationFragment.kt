package com.scb10x.xboard.features.topic.presentation

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.failure
import com.scb10x.xboard.core.extension.hideKeyboard
import com.scb10x.xboard.core.extension.observe
import com.scb10x.xboard.core.extension.useDefaultHorizontalLayout
import com.scb10x.xboard.core.platform.BaseFragment
import kotlinx.android.synthetic.main.fragment_create_topic.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import retrofit2.HttpException
import java.lang.IllegalArgumentException
import java.net.SocketException

class TopicCreationFragment : BaseFragment() {
    companion object {
        private const val PARAM_CATEGORY = "param_category"

        fun forCategory(categoryView: CategoryView): TopicCreationFragment {
            val topicCreationFragment = TopicCreationFragment()
            val bundle = Bundle()
            bundle.putParcelable(PARAM_CATEGORY, categoryView)

            topicCreationFragment.arguments = bundle

            return topicCreationFragment
        }
    }


    override fun layoutId(): Int = R.layout.fragment_create_topic

    private val topicViewModel: TopicViewModel by viewModel()

    private val categoryViewModel: CategoryViewModel by viewModel()
    private val categoryAdapter: SelectableCategoryAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(topicViewModel) {
            observe(createdTopic, ::handleCreatedTopic)
            observe(isLoading, ::handleProgress)
            failure(error, ::handleFailure)
        }

        with(categoryViewModel) {
            observe(isLoading, ::handleProgress)
            observe(categories, ::renderCategories)
            failure(error, {
                it?.printStackTrace()
            })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (firstTimeCreated(savedInstanceState)) {
            initializeView((arguments?.get(PARAM_CATEGORY) as CategoryView))
        } else {
            initializeView((arguments!![PARAM_CATEGORY] as CategoryView))
        }

    }

    private fun initializeView(categoryView: CategoryView) {
        post_button.setOnClickListener {
            postTopic()
        }

        initializeList(categoryView)
        categoryViewModel.loadCategories()
    }

    private fun initializeList(categoryView: CategoryView) {
        category_list.useDefaultHorizontalLayout(activity?.baseContext)
        category_list.adapter = categoryAdapter

        categoryAdapter.selectedItem = categoryView
        categoryAdapter.selectedPosition =
            if (categoryView.id <= 0) 0
            else categoryView.id.toInt() - 1
    }

    private fun postTopic() {
        val title = topic_title_edit_text.text.toString()
        val message = topic_message_edit_text.text.toString()

        topicViewModel.saveTopic(
            TopicView(
                title = title,
                message = message,
                categoryID = categoryAdapter.selectedItem.id
            ).toTopic()
        )
    }

    private fun handleCreatedTopic(topicView: TopicView?) {
        with(activity!!) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun renderCategories(categories: List<CategoryView>?) {
        categoryAdapter.collection = categories.orEmpty()
    }

    private fun handleFailure(error: Throwable?) {
        topic_message_edit_text.requestFocus()
        topic_message_edit_text.hideKeyboard(activity)

        when (error) {
            is IllegalArgumentException -> notify(R.string.topic_create_error_empty)
            is SocketException, is HttpException -> notifyWithAction(
                R.string.network_error,
                R.string.action_retry
            ) { postTopic() }
            else -> notify(R.string.unknown_error)
        }
    }
}