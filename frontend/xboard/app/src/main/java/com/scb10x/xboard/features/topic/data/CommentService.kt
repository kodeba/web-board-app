package com.scb10x.xboard.features.topic.data

import com.scb10x.xboard.core.data.BaseService
import io.reactivex.Flowable
import retrofit2.Retrofit

class CommentService(retrofit: Retrofit) : BaseService<CommentApi>(retrofit), CommentApi {

    override val api: CommentApi = createApi()

    override fun comments(topicID: Long, offset: Int): Flowable<List<CommentEntity>> = api.comments(topicID, offset)

    override fun createComment(commentEntity: CommentEntity): Flowable<CommentEntity> = api.createComment(commentEntity)
}