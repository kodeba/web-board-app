package com.scb10x.xboard.core.platform

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {

    protected val disposable = CompositeDisposable()
    var error : MutableLiveData<Throwable> = MutableLiveData()
    var isLoading : MutableLiveData<Boolean> = MutableLiveData()

    protected fun handleError(throwable: Throwable?) {
        throwable?.printStackTrace()
        error.value = throwable
    }

    protected fun loading() {
        isLoading.value = true
    }

    protected fun done() {
        isLoading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}