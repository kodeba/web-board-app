package com.scb10x.xboard.features.topic.presentation

import android.os.Bundle
import android.view.View
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.failure
import com.scb10x.xboard.core.extension.observe
import com.scb10x.xboard.core.extension.useDefaultVerticleLayout
import com.scb10x.xboard.core.platform.BaseActivity
import com.scb10x.xboard.core.platform.BaseFragment
import kotlinx.android.synthetic.main.fragment_topic.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class TopicCategoryFragment : BaseFragment() {

    companion object {
        private const val PARAM_CATEGORY = "param_category"

        fun forCategory(categoryView: CategoryView): TopicCategoryFragment {
            val topicCategoryFragment = TopicCategoryFragment()
            val bundle = Bundle()
            bundle.putParcelable(PARAM_CATEGORY, categoryView)

            topicCategoryFragment.arguments = bundle

            return topicCategoryFragment
        }
    }

    override fun layoutId(): Int = R.layout.fragment_topic

    private val topicViewModel: TopicViewModel by viewModel()
    private val topicsAdapter: TopicsAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(topicViewModel) {
            observe(isLoading, ::handleProgress)
            observe(topics, ::renderTopics)
            failure(error, {
                it?.printStackTrace()
            })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (firstTimeCreated(savedInstanceState)) {
            initializeView((arguments?.get(PARAM_CATEGORY) as CategoryView))
        } else {
            initializeView((arguments!![PARAM_CATEGORY] as CategoryView))
        }
    }

    private fun initializeView(categoryView: CategoryView){
        initializeToolbar(categoryView)
        initializeList(categoryView)
        initializeButton(categoryView)
    }

    private fun initializeToolbar(categoryView: CategoryView) {
        category_group.visibility = View.GONE

        (activity as BaseActivity).supportActionBar?.title = categoryView.title
    }

    private fun initializeList(categoryView: CategoryView) {
        topic_list.useDefaultVerticleLayout(activity?.baseContext)
        topic_list.adapter = topicsAdapter
        topicsAdapter.clickListener = { navigator.showTopicDetail(activity!!, it) }

        topicViewModel.loadTopics(categoryID = categoryView.id)
    }

    private fun initializeButton(categoryView: CategoryView) {
        create_button.setOnClickListener { navigator.showTopicCreation(activity!!, categoryView) }
    }

    private fun renderTopics(topics: List<TopicView>?) {
        topicsAdapter.collection = topics.orEmpty()
    }
}