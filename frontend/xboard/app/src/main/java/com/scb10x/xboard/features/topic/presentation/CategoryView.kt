package com.scb10x.xboard.features.topic.presentation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class CategoryView(
    val id: Long = 0L,
    val title: String = "",
    val description: String = "",
    val created: Date? = null
) : Parcelable