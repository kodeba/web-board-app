package com.scb10x.xboard.features.topic.data

import io.reactivex.Flowable
import retrofit2.http.GET

interface CategoryApi {
    companion object {
        private const val CATEGORY_ROOT = "category"
    }

    @GET(CATEGORY_ROOT)
    fun categories(): Flowable<List<CategoryEntity>>
}