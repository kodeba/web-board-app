package com.scb10x.xboard.core.navigation

import androidx.fragment.app.FragmentActivity
import com.scb10x.xboard.features.topic.presentation.*

class Navigator {

    fun showTopicDetail(context: FragmentActivity, topicView: TopicView) =
        context.startActivity(TopicDetailActivity.callingIntent(context, topicView))

    fun showTopicCreation(context: FragmentActivity, categoryView: CategoryView) =
        context.startActivityForResult(TopicCreationActivity.callingIntent(context, categoryView), 1)

    fun showTopicCategory(context: FragmentActivity, categoryView: CategoryView) =
        context.startActivity(TopicCategoryActivity.callingIntent(context, categoryView))
}