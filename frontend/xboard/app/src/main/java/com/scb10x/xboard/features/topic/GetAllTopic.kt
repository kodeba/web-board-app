package com.scb10x.xboard.features.topic

import com.scb10x.xboard.core.interactor.UseCase
import com.scb10x.xboard.features.topic.data.TopicRepository
import io.reactivex.Flowable

class GetAllTopic(override val repository: TopicRepository) : UseCase<GetAllTopic.Param, Flowable<List<Topic>>>(repository) {
    override fun run(param: Param): Flowable<List<Topic>> = repository.topics(param.offset, param.categoryID)

    class Param(val offset : Int = 0, val categoryID : Long = 0)
}