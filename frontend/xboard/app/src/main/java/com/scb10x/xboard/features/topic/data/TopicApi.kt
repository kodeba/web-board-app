package com.scb10x.xboard.features.topic.data

import io.reactivex.Flowable
import retrofit2.http.*

interface TopicApi {
    companion object {
        private const val OFFSET = 10
        private const val PARAM_OFFSET = "offset"
        private const val PARAM_CATEGORY_ID = "category_id"
        private const val PARAM_TOPIC_ID = "topicID"
        private const val TOPIC_ROOT = "topic"
        private const val TOPIC_BY_ID = "$TOPIC_ROOT/{$PARAM_TOPIC_ID}"
        private const val TOPIC_LIST = "$TOPIC_ROOT?"
    }

    @GET(TOPIC_LIST)
    fun topics(
        @Query(PARAM_OFFSET) offset: Int = 0,
        @Query(PARAM_CATEGORY_ID) categoryID: Long = 0L
    ): Flowable<List<TopicEntity>>

    @GET(TOPIC_BY_ID)
    fun topic(@Path(PARAM_TOPIC_ID) topicID: Long): Flowable<TopicEntity>

    @POST(TOPIC_ROOT)
    fun createTopic(@Body topicEntity: TopicEntity): Flowable<TopicEntity>
}