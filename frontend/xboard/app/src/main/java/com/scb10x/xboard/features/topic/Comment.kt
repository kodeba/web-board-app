package com.scb10x.xboard.features.topic

import com.google.gson.annotations.SerializedName
import com.scb10x.xboard.core.extension.decode
import com.scb10x.xboard.features.topic.data.CommentEntity
import com.scb10x.xboard.features.topic.presentation.CommentView
import java.util.*

data class Comment(
    val id: Long = 0L,
    val message: String,
    val ipAddress: String = "0.0.0.0",
    val topicID: Long,
    val created: Date? = null
) {
    fun toEntity() = CommentEntity(id, message, ipAddress, topicID, created)

    fun toView() = CommentView(id, message.decode(), ipAddress, topicID, created)
}