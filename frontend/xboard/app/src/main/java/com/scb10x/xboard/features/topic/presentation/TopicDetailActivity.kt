package com.scb10x.xboard.features.topic.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.scb10x.xboard.core.platform.BaseActivity
import com.scb10x.xboard.core.platform.BaseFragment
import kotlinx.android.synthetic.main.toobar.*

class TopicDetailActivity : BaseActivity() {

    companion object {
        private const val INTENT_PARAM_TOPIC = "intent_param_topic"

        fun callingIntent(context: Context, topicView: TopicView): Intent {
            val intent = Intent(context, TopicDetailActivity::class.java)
            intent.putExtra(INTENT_PARAM_TOPIC, topicView)
            return intent
        }
    }

    override fun fragment(): BaseFragment = TopicDetailFragment.forTopic(intent.getParcelableExtra(INTENT_PARAM_TOPIC))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(supportActionBar!!){
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        toolbar.setNavigationOnClickListener{ finish() }
    }
}