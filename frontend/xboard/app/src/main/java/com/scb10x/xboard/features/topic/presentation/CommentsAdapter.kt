package com.scb10x.xboard.features.topic.presentation

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.inflate
import com.scb10x.xboard.core.extension.timeAgo
import kotlinx.android.synthetic.main.list_item_comment.view.*
import kotlin.properties.Delegates

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {

    internal var collection: List<CommentView> by Delegates.observable(emptyList(),
        { _, _, _ -> notifyDataSetChanged() })

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.list_item_comment))
    }

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(collection[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(commentView: CommentView) {

            with(itemView){
                comment_message.text = commentView.message
                comment_owner.text = commentView.ipAddress
                comment_last_update.text = commentView.created?.timeAgo()
            }
        }
    }
}