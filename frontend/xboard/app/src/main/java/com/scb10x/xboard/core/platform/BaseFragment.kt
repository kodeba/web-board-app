package com.scb10x.xboard.core.platform

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.scb10x.xboard.R
import com.scb10x.xboard.core.extension.appContext
import com.scb10x.xboard.core.extension.viewContainer
import com.scb10x.xboard.core.navigation.Navigator
import kotlinx.android.synthetic.main.toobar.*
import org.koin.android.ext.android.inject

abstract class BaseFragment : Fragment() {

    internal val navigator: Navigator by inject()

    abstract fun layoutId(): Int

    open fun onBackPressed() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layoutId(), container, false)

    internal fun firstTimeCreated(savedInstanceState: Bundle?) = savedInstanceState == null

    internal fun showProgress() = progressStatus(View.VISIBLE)

    internal fun hideProgress() = progressStatus(View.GONE)

    internal fun handleProgress(isLoading: Boolean?) {
        when (isLoading) {
            true -> showProgress()
            false -> hideProgress()
        }
    }

    private fun progressStatus(viewStatus: Int) =
        with(activity) { if (this is BaseActivity) this.progress.visibility = viewStatus }

    internal fun notify(@StringRes message: Int) =
        Snackbar.make(viewContainer, message, Snackbar.LENGTH_SHORT).show()

    internal fun notifyWithAction(@StringRes message: Int, @StringRes actionText: Int, action: () -> Any) {
        val snackBar = Snackbar.make(viewContainer, message, Snackbar.LENGTH_INDEFINITE)
        snackBar.setAction(actionText) { action.invoke() }
        snackBar.setActionTextColor(
            ContextCompat.getColor(
                appContext,
                R.color.colorPrimary
            )
        )
        snackBar.show()
    }
}