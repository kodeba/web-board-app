package com.scb10x.xboard.core.extension

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.material.textfield.TextInputEditText


fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)

fun RecyclerView.useDefaultVerticleLayout(c: Context?) {
    this.layoutManager = LinearLayoutManager(c)
}

fun RecyclerView.useDefaultHorizontalLayout(c: Context?) {
    this.layoutManager = LinearLayoutManager(c, LinearLayoutManager.HORIZONTAL, false)
}

fun TextInputEditText.hideKeyboard(activity: Activity?) = hideKeyboard(activity, this)

fun EditText.hideKeyboard(activity: Activity?) = hideKeyboard(activity, this)

private fun hideKeyboard(activity: Activity?, view: View) {
    (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view.windowToken, 0)
}