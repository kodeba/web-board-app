package com.scb10x.xboard.core.data

import retrofit2.Retrofit

abstract class BaseService<T>(val retrofit: Retrofit) {

    abstract val api : T

    inline fun <reified T : Any> createApi(): T = retrofit.create(T::class.java)
}