package com.scb10x.xboard.core.di

import com.scb10x.xboard.features.topic.*
import com.scb10x.xboard.features.topic.presentation.*
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val topicModule = module {
    factory { GetAllTopic(get()) }
    factory { GetTopicByID(get()) }
    factory { CreateTopic(get()) }
    viewModel { TopicViewModel(get(), get(), get()) }
    factory { TopicsAdapter() }

    factory { CreateComment(get()) }
    factory { GetTopicComments(get()) }
    viewModel { CommentViewModel(get(), get()) }
    factory { CommentsAdapter() }

    factory { GetAllCategory(get()) }
    viewModel { CategoryViewModel(get()) }
    factory { CategoryAdapter() }
    factory { SelectableCategoryAdapter() }
}