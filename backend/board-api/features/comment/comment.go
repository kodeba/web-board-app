package comment

import (
	"board-api/core/database"
	"board-api/core/helper"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type Comment struct {
	ID        uint64    `gorm:"primary_key" json:"id"`
	Message   string    `json:"message"`
	IPAddress string    `gorm:"default:'0.0.0.0'" json:"ip_address"`
	TopicID   uint64    `json:"topic_id"`
	Created   time.Time `gorm:"default:current_timestamp" json:"created"`
}

func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", GetAllComments)
	router.Post("/", CreateComment)
	return router
}

func GetAllComments(w http.ResponseWriter, r *http.Request) {
	var comments []Comment

	topicID, err := strconv.ParseInt(r.URL.Query().Get("topic_id"), 10, 64)
	if err != nil || topicID <= 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	offset, err := strconv.ParseInt(r.URL.Query().Get("offset"), 10, 64)
	if err != nil {
		offset = 0
	}

	database.Connection.
		Limit(999).
		Offset(offset).
		Order("created DESC").
		Where(&Comment{TopicID: uint64(topicID)}).
		Find(&comments)

	render.JSON(w, r, comments)
}

func CreateComment(w http.ResponseWriter, r *http.Request) {
	ip := helper.GetIPAddress(r)
	comment := &Comment{IPAddress: ip}

	err := json.NewDecoder(r.Body).Decode(comment)

	if err != nil || comment.TopicID == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if e := database.Connection.Create(comment).Error; e != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	render.JSON(w, r, comment)
}
