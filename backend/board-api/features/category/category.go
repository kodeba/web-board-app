package category

import (
	"board-api/core/database"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

type Category struct {
	ID          uint64    `gorm:"primary_key;foreignkey:CategoryID;association_foreignkey:ID" json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Created     time.Time `gorm:"default:current_timestamp" json:"created"`
}

func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", GetAllCategories)
	return router
}

func GetAllCategories(w http.ResponseWriter, r *http.Request) {
	var categories []Category

	database.Connection.Find(&categories)

	render.JSON(w, r, categories)
}
