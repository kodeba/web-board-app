package topic

import (
	"board-api/core/helper"
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/jinzhu/gorm"

	"board-api/core/database"
)

type Topic struct {
	ID         uint64    `gorm:"primary_key" json:"id"`
	Title      string    `json:"title"`
	Message    string    `json:"message"`
	IPAddress  string    `gorm:"default:'0.0.0.0'" json:"ip_address"`
	CategoryID uint64    `json:"category_id"`
	Created    time.Time `gorm:"default:current_timestamp" json:"created"`
}

type DisplayTopic struct {
	Topic
	TotalComment uint `json:"total_comment"`
}

func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", GetAllTopic)
	router.Get("/{todoID}", GetTopicByID)
	router.Post("/", CreateTopic)
	return router
}

func GetAllTopic(w http.ResponseWriter, r *http.Request) {
	categoryID, err := strconv.ParseInt(r.URL.Query().Get("category_id"), 10, 64)
	if err != nil {
		categoryID = 0
	}

	offset, err := strconv.ParseInt(r.URL.Query().Get("offset"), 10, 64)
	if err != nil {
		offset = 0
	}

	var displayTopic []DisplayTopic

	database.Connection.
		Limit(999).
		Offset(offset).
		Raw(`SELECT t.id, 
		t.title, 
		t.message, 
		t.ip_address, 
		t.category_id, 
		Count(c.id) AS total_comment, 
		Max(CASE 
			  WHEN c.created IS NOT NULL THEN c.created 
			  ELSE t.created 
			END)    
		AS created 
		FROM topics t 
				LEFT JOIN comments c 
					ON t.id = c.topic_id 
		WHERE  	CASE 
					WHEN ? > 0 THEN t.category_id = ? 
					ELSE true 
				END 
		GROUP  BY t.id 
		ORDER  BY created DESC `, categoryID, categoryID).
		Scan(&displayTopic)

	render.JSON(w, r, displayTopic)
}

func GetTopicByID(w http.ResponseWriter, r *http.Request) {
	todoID, err := strconv.ParseInt(chi.URLParam(r, "todoID"), 10, 64)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		var topic Topic
		database.Connection.
			Preload("Comments", func(db *gorm.DB) *gorm.DB {
				return db.Order("comments.created DESC")
			}).
			Where(&Topic{ID: uint64(todoID)}).
			Find(&topic)
		render.JSON(w, r, topic)
	}
}

func CreateTopic(w http.ResponseWriter, r *http.Request) {
	ip := helper.GetIPAddress(r)
	topic := &Topic{IPAddress: ip}

	if err := json.NewDecoder(r.Body).Decode(topic); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := database.Connection.Create(topic).Error; err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	render.JSON(w, r, topic)
}
