package helper

import (
	"net"
	"net/http"
)

func GetIPAddress(r *http.Request) string {
	ip, _, e := net.SplitHostPort(r.RemoteAddr)
	if e != nil || ip == "::1" {
		ip = "0.0.0.0"
	}
	return ip
}
