package config

import (
	"log"

	"github.com/spf13/viper"
)

type DatabaseConfig struct {
	Host     string
	Port     string
	Username string
	Password string
}

var DBConfig DatabaseConfig

func LoadConfig() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Cannot reading config : %s", err.Error())
	}

	err := viper.UnmarshalKey("database", &DBConfig)

	if err != nil {
		log.Fatalf("Cannot decode config : %s", err.Error())
	}

	log.Printf("Host : %s\n", DBConfig.Host)
	log.Printf("Port : %s\n", DBConfig.Port)
	log.Printf("Username : %s\n", DBConfig.Username)
	log.Printf("Password : %s\n", DBConfig.Password)

}
