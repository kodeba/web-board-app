package database

import (
	"board-api/core/config"

	"github.com/jinzhu/gorm"
)

var Connection *gorm.DB

func Open() error {
	var err error
	Connection, err = gorm.Open("mysql",
		config.DBConfig.Username+":"+
			config.DBConfig.Password+"@tcp("+
			config.DBConfig.Host+":"+
			config.DBConfig.Port+
			")/board10x?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		return err
	}

	return nil
}

func Close() error {
	return Connection.Close()
}
