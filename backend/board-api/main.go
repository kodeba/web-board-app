package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"board-api/core/config"
	"board-api/core/database"
	"board-api/features/category"
	"board-api/features/comment"
	"board-api/features/topic"
)

func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON), // Set content-Type headers as application/json
		middleware.Logger,          // Log API request calls
		middleware.DefaultCompress, // Compress results, mostly gzipping assets and json
		middleware.RedirectSlashes, // Redirect slashes to no slash URL versions
		middleware.Recoverer,       // Recover from panics without crashing server
	)

	router.Route("/api", func(r chi.Router) {
		r.Mount("/topic", topic.Routes())
		r.Mount("/comment", comment.Routes())
		r.Mount("/category", category.Routes())
	})

	return router
}

func dbConnection() {
	if err := database.Open(); err != nil {
		log.Panicf("DB Connection Fail : %s\n", err.Error())
	} else {
		log.Printf("DB Connection : Success")
	}

	database.Connection.AutoMigrate(
		&topic.Topic{},
		&comment.Comment{},
		&category.Category{},
	)
}

func main() {
	config.LoadConfig()

	dbConnection()

	router := Routes()

	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route) // Walk and print out all routes
		return nil
	}
	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("Logging err: %s\n", err.Error()) // panic if there is an error
	}

	log.Fatal(http.ListenAndServe(":8080", router)) // Note, the port is usually gotten from the environment.
}
