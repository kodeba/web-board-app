/*TABLE CREATION*/
CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `topics` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT '0.0.0.0',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `category_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `topics_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT '0.0.0.0',
  `topic_id` bigint(20) unsigned DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `topic_id` (`topic_id`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*INSERT MANDATORY DATA*/
INSERT INTO `categories` (`id`, `title`, `description`, `created`)
VALUES
	(1, 'General', 'General topic that people can discuss anything around the world', now()),
	(2, 'Technology', 'Technology is never sleep', now()),
	(3, 'Life', 'LIVE YOUR LIFE', now()),
	(4, 'Education', 'Lifelong learnner are welcome', now()),
	(5, 'Fashion', 'Show your style', now()),
	(6, 'Art', 'Art is long, Life is short', now());